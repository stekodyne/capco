// Author: Steffen T Kory
// Date: 2017-05-19
// Version: 0.0.1-SNAPSHOT
// Licensed under the MIT License. See LICENSE file in the project root for full license information.
const util = require('util');
const assert = require('assert');
const nearley = require("nearley");
const grammar = require('../lib/capco.js');

describe('CAPCO', function() {

  describe('banner', function() {
    var parser;

    beforeEach(function() {
      parser = new nearley.Parser(grammar.ParserRules, grammar.ParserStart);
    });

    afterEach(function() {
      parser.finish();
    });

    it(
      'should parse a "//JOINT SECRET CAN GBR USA//REL TO USA, CAN, GBR" marking',
      function() {
        var expected = [{
          classification: {
            markingType: 'joint',
            joint: {
              marking: 'SECRET',
              countries: ['CAN', 'GBR', 'USA']
            }
          },
          dissemination: {
            relto: {
              countries: ['USA', 'CAN', 'GBR']
            }
          }
        }];
        try {
          parser.feed(
            "//JOINT SECRET CAN GBR USA//REL TO USA, CAN, GBR");
          var actual = parser.results;
          assert.deepEqual(actual, expected);
        } catch (parseError) {
          throw new Error(parseError);
        }
      });

    it('should parse a "SECRET//REL TO USA, GBR" marking', function() {
      var expected = [{
        classification: {
          markingType: 'us',
          us: {
            marking: 'SECRET'
          }
        },
        dissemination: {
          relto: {
            countries: ['USA', 'GBR']
          }
        }
      }];
      try {
        parser.feed("SECRET//REL TO USA, GBR");
        var actual = parser.results;
        assert.deepEqual(actual, expected);
      } catch (parseError) {
        throw new Error(parseError);
      }
    });

    it('should parse a "SECRET//NOFORN//REL TO USA, GBR" marking',
      function() {
        var expected = [{
          classification: {
            markingType: 'us',
            us: {
              marking: 'SECRET'
            }
          },
          dissemination: {
            noforn: true,
            relto: {
              countries: ['USA', 'GBR']
            }
          }
        }];
        try {
          parser.feed("SECRET//NOFORN//REL TO USA, GBR");
          var actual = parser.results;
          assert.deepEqual(actual, expected);
        } catch (parseError) {
          throw new Error(parseError);
        }
      });

    it(
      'should parse an "UNCLASSIFIED//FOUO//REL TO UNITED STATES" marking',
      function() {
        var expected = [{
          classification: {
            markingType: 'us',
            us: {
              marking: 'UNCLASSIFIED'
            }
          },
          dissemination: {
            fouo: true,
            relto: {
              countries: ['UNITED STATES']
            }
          }
        }];
        try {
          parser.feed("UNCLASSIFIED//FOUO//REL TO UNITED STATES");
          var actual = parser.results;
          assert.deepEqual(actual, expected);
        } catch (parseError) {
          throw new Error(parseError);
        }
      });

    it(
      'should parse an "UNCLASSIFIED//FOUO//AUTHORIZED FOR RELEASE TO UNITED STATES, ARUBA, BERMUDA, CANADA" marking',
      function() {
        var expected = [{
          classification: {
            markingType: 'us',
            us: {
              marking: 'UNCLASSIFIED'
            }
          },
          dissemination: {
            fouo: true,
            relto: {
              countries: ['UNITED STATES', 'ARUBA', 'BERMUDA',
                'CANADA'
              ]
            }
          }
        }];
        try {
          parser.feed(
            "UNCLASSIFIED//FOUO//AUTHORIZED FOR RELEASE TO UNITED STATES, ARUBA, BERMUDA, CANADA"
          );
          var actual = parser.results;
          assert.deepEqual(actual, expected);
        } catch (parseError) {
          throw new Error(parseError);
        }
      });
  });

  describe('potion', function() {
    var parser;

    beforeEach(function() {
      parser = new nearley.Parser(grammar.ParserRules, grammar.ParserStart);
    });

    afterEach(function() {
      parser.finish();
    });

    it('should parse a "(S//REL USA, GBR)" marking', function() {
      var expected = [{
        classification: {
          markingType: 'us',
          us: {
            marking: 'S'
          }
        },
        dissemination: {
          relto: {
            countries: ['USA', 'GBR']
          }
        }
      }];
      try {
        parser.feed("(S//REL USA, GBR)");
        var actual = parser.results;
        assert.deepEqual(actual, expected);
      } catch (parseError) {
        throw new Error(parseError);
      }
    });

    it('should parse a "(S//NF)" marking', function() {
      var expected = [{
        classification: {
          markingType: 'us',
          us: {
            marking: 'S'
          }
        },
        dissemination: {
          noforn: true
        }
      }];
      try {
        parser.feed("(S//NF)");
        var actual = parser.results;
        assert.deepEqual(actual, expected);
      } catch (parseError) {
        throw new Error(parseError);
      }
    });

    it('should parse a "(C)" marking', function() {
      var expected = [{
        classification: {
          markingType: 'us',
          us: {
            marking: 'C'
          }
        }
      }];
      try {
        parser.feed("(C)");
        var actual = parser.results;
        assert.deepEqual(actual, expected);
      } catch (parseError) {
        throw new Error(parseError);
      }
    });

    it('should parse a "(S//NF//REL USA, GBR)" marking', function() {
      var expected = [{
        classification: {
          markingType: 'us',
          us: {
            marking: 'S'
          }
        },
        dissemination: {
          noforn: true,
          relto: {
            countries: ['USA', 'GBR']
          }
        }
      }];
      try {
        parser.feed("(S//NF//REL USA, GBR)");
        var actual = parser.results;
        assert.deepEqual(actual, expected);
      } catch (parseError) {
        throw new Error(parseError);
      }
    });

    it('should parse an "(U//FOUO//REL USA)" marking', function() {
      var expected = [{
        classification: {
          markingType: 'us',
          us: {
            marking: 'U'
          }
        },
        dissemination: {
          fouo: true,
          relto: {
            countries: ['USA']
          }
        }
      }];
      try {
        parser.feed("(U//FOUO//REL USA)");
        var actual = parser.results;
        assert.deepEqual(actual, expected);
      } catch (parseError) {
        throw new Error(parseError);
      }
    });

    it(
      'should parse a "(//JOINT U CAN USA//REL USA, CAN)" marking',
      function() {
        var expected = [{
          classification: {
            markingType: 'joint',
            joint: {
              marking: 'U',
              countries: ['CAN', 'USA']
            }
          },
          dissemination: {
            relto: {
              countries: ['USA', 'CAN']
            }
          }
        }];
        try {
          parser.feed("(//JOINT U CAN USA//REL USA, CAN)");
          var actual = parser.results;
          assert.deepEqual(actual, expected);
        } catch (parseError) {
          throw new Error(parseError);
        }
      });
  });

  describe('invalid', function() {
    var parser;

    beforeEach(function() {
      parser = new nearley.Parser(grammar.ParserRules, grammar.ParserStart);
    });

    afterEach(function() {
      parser.finish();
    });

    it(
      'should fail trying to parse an invalid "TURTLES//REL TO USA, GBR" classification marking',
      function() {
        try {
          parser.feed("TURTLES//REL TO USA, GBR");
        } catch (parseError) {
          assert(parseError);
        }
      });

    it(
      'should fail trying to parse an invalid "(T//REL USA, GBR)" classification marking',
      function() {
        try {
          parser.feed("(T//REL USA, GBR)");
        } catch (parseError) {
          assert(parseError);
        }
      });
  });
});
