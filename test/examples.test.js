// Author: Steffen T Kory
// Date: 2017-05-19
// Version: 0.0.1-SNAPSHOT
// Licensed under the MIT License. See LICENSE file in the project root for full license information.
const util = require('util');
const assert = require('assert');
const nearley = require("nearley");
const grammar = require('../lib/capco.js');

describe('CAPCO', function () {

    // Examples from CAPCO Reg v5-1
    describe('basic examples', function () {
        var parser;

        beforeEach(function () {
            parser = new nearley.Parser(grammar.ParserRules, grammar.ParserStart);
        });

        afterEach(function () {
            parser.finish();
        });

        it('should parse a "CONFIDENTIAL//REL TO USA, FVEY/RELIDO" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'CONFIDENTIAL'
                        }
                    },
                    dissemination: {
                        relto: {
                            countries: ['USA', 'FVEY']
                        },
                        relido: true
                    }
                }];
                try {
                    parser.feed("CONFIDENTIAL//REL TO USA, FVEY/RELIDO");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(C//REL/RELIDO)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'C'
                    }
                },
                dissemination: {
                    relto: true,
                    relido: true
                }
            }];
            try {
                parser.feed("(C//REL/RELIDO)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it(
            'should parse a "TOP SECRET//SI-GAMMA/TALENT KEYHOLE//RISK SENSITIVE/ORIGINATOR CONTROLLED/NOFORN" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'TOP SECRET'
                        }
                    },
                    sci: {
                        "si-g": true,
                        tk: true
                    },
                    dissemination: {
                        rsen: true,
                        orcon: true,
                        noforn: true
                    }
                }];
                try {
                    parser.feed(
                        "TOP SECRET//SI-GAMMA/TALENT KEYHOLE//RISK SENSITIVE/ORIGINATOR CONTROLLED/NOFORN"
                    );
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it(
            'should parse a "TOP SECRET//SI-G/TK//RSEN/ORCON/NOFORN" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'TOP SECRET'
                        }
                    },
                    sci: {
                        "si-g": true,
                        tk: true
                    },
                    dissemination: {
                        rsen: true,
                        orcon: true,
                        noforn: true
                    }
                }];
                try {
                    parser.feed("TOP SECRET//SI-G/TK//RSEN/ORCON/NOFORN");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(TS//SI-G/TK//RS/OC/NF)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'TS'
                    }
                },
                sci: {
                    "si-g": true,
                    tk: true
                },
                dissemination: {
                    rsen: true,
                    orcon: true,
                    noforn: true
                }
            }];
            try {
                parser.feed("(TS//SI-G/TK//RS/OC/NF)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "TOP SECRET//SI-ABC-DEF//ORCON/NOFORN" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'TOP SECRET'
                        }
                    },
                    sci: {
                        si: { compartments: ['-ABC', '-DEF'] }
                    },
                    dissemination: {
                        orcon: true,
                        noforn: true
                    }
                }];
                try {
                    parser.feed("TOP SECRET//SI-ABC-DEF//ORCON/NOFORN");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(TS//SI-ABC-DEF//OC/NF)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'TS'
                    }
                },
                sci: {
                    si: { compartments: ['-ABC', '-DEF'] }
                },
                dissemination: {
                    orcon: true,
                    noforn: true
                }
            }];
            try {
                parser.feed("(TS//SI-ABC-DEF//OC/NF)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it(
            'should parse a "TOP SECRET//SI-G ABCD EFGH-XYZ//ORCON/NOFORN" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'TOP SECRET'
                        }
                    },
                    sci: {
                        si: { gamma: ['ABCD', 'EFGH'],
                            compartments: ['-XYZ']
                        }
                    },
                    dissemination: {
                        orcon: true,
                        noforn: true
                    }
                }];
                try {
                    parser.feed("TOP SECRET//SI-G ABCD EFGH-XYZ//ORCON/NOFORN");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(TS//SI-G ABCD EFGH-XYZ//OC/NF)" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'TS'
                        }
                    },
                    sci: {
                        si: { gamma: ['ABCD', 'EFGH'],
                            compartments: ['-XYZ']
                        }
                    },
                    dissemination: {
                        orcon: true,
                        noforn: true
                    }
                }];
                try {
                    parser.feed("(TS//SI-G ABCD EFGH-XYZ//OC/NF)");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it(
            'should parse a "TOP SECRET//AUNPUB/SI/TALENT KEYHOLE/XUNPUB//NOFORN" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed(
                        "TOP SECRET//AUNPUB/SI/TALENT KEYHOLE/XUNPUB//NOFORN");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "TOP SECRET//ANB/SI/TK/XNB//NOFORN" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed("TOP SECRET//ANB/SI/TK/XNB//NOFORN");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(TS//ANB/SI/TK/XNB//NF)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("(TS//ANB/SI/TK/XNB//NF)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "UNCLASSIFIED//FOUO" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("UNCLASSIFIED//FOUO");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it(
            'should parse a "TOP SECRET//SAR-BUTTER POPCORN-123/CANDY APPLE-XYZ YYY//NOT RELEASABLE TO FOREIGN NATIONALS" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed(
                        "TOP SECRET//SAR-BUTTER POPCORN-123/CANDY APPLE-XYZ YYY//NOT RELEASABLE TO FOREIGN NATIONALS"
                    );
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it(
            'should parse a "TOP SECRET//SAR-BP-123/CA-XYZ YYY//NOFORN" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed("TOP SECRET//SAR-BP-123/CA-XYZ YYY//NOFORN");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(TS//SAR-BP-123/CA-XYZ YYY//NF)" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed("(TS//SAR-BP-123/CA-XYZ YYY//NF)");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "TOP SECRET//RD-CNWDI//NOFORN" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed("TOP SECRET//RD-CNWDI//NOFORN");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(TS//RD-CNWDI//NF)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("(TS//RD-CNWDI//NF)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it(
            'should parse a "SECRET//FRD-SIGMA 14 18//REL TO USA, ACGU" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed("SECRET//FRD-SIGMA 14 18//REL TO USA, ACGU");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(S//FRD-SIGMA 14 18//REL)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("(S//FRD-SIGMA 14 18//REL)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "//COSMIC TOP SECRET//BOHEMIA" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed("//COSMIC TOP SECRET//BOHEMIA");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(//CTS//BOHEMIA)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("(//CTS//BOHEMIA)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "//DEU SECRET//NOFORN" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("//DEU SECRET//NOFORN");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "(//DEU S//NF)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("(//DEU S//NF)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "//NATO SECRET//ATOMAL//ORCON" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed("//NATO SECRET//ATOMAL//ORCON");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(//NS//ATOMAL//OC)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("(//NS//ATOMAL//OC)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it(
            'should parse a "//JOINT SECRET CAN GBR USA//REL TO USA, CAN, GBR" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'joint',
                        joint: {
                            marking: 'SECRET',
                            countries: ['CAN', 'GBR', 'USA']
                        }
                    },
                    dissemination: {
                        relto: {
                            countries: ['USA', 'CAN', 'GBR']
                        }
                    }
                }];
                try {
                    parser.feed(
                        "//JOINT SECRET CAN GBR USA//REL TO USA, CAN, GBR");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(//JOINT S//REL)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'joint',
                    joint: {
                        marking: 'S',
                        countries: []
                    }
                },
                dissemination: {
                    relto: true
                }
            }];
            try {
                parser.feed("(//JOINT S//REL)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it(
            'should parse a "TOP SECRET//FGI DEU GBR//REL TO USA, DEU, GBR" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed("TOP SECRET//FGI DEU GBR//REL TO USA, DEU, GBR");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it(
            'should parse a "(TS//FGI DEU GBR//REL TO USA, DEU, GBR)" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'S'
                        }
                    },
                    dissemination: {
                        noforn: true,
                        propin: true
                    }
                }];
                try {
                    parser.feed("(TS//FGI DEU GBR//REL TO USA, DEU, GBR)");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "SECRET//TK//FGI//NOFORN" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("SECRET//TK//FGI//NOFORN");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "(//FGI S//NF)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("(//FGI S//NF)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "SECRET//REL TO USA, DEU/RELIDO" marking',
            function () {
                var expected = [{
                    classification: {
                        markingType: 'us',
                        us: {
                            marking: 'SECRET'
                        }
                    },
                    dissemination: {
                        relto: {
                            countries: ['USA', 'DEU']
                        },
                        relido: true
                    }
                }];
                try {
                    parser.feed("SECRET//REL TO USA, DEU/RELIDO");
                    var actual = parser.results;
                    assert.deepEqual(actual, expected);
                } catch (parseError) {
                    throw new Error(parseError);
                }
            });

        it('should parse a "(S//REL/RELIDO)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    relto: true,
                    relido: true
                }
            }];
            try {
                parser.feed("(S//REL/RELIDO)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "SECRET//NOFORN" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'SECRET'
                    }
                },
                dissemination: {
                    noforn: true
                }
            }];
            try {
                parser.feed("SECRET//NOFORN");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "(S//NF)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true
                }
            }];
            try {
                parser.feed("(S//NF)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "SECRET//NOFORN/PROPIN" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'SECRET'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("SECRET//NOFORN/PROPIN");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "(S//NF/PR)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'S'
                    }
                },
                dissemination: {
                    noforn: true,
                    propin: true
                }
            }];
            try {
                parser.feed("(S//NF/PR)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "UNCLASSIFIED//SSI" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'UNCLASSIFIED'
                    }
                },
                nonic: {
                    ssi: true
                }
            }];
            try {
                parser.feed("UNCLASSIFIED//SSI");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });

        it('should parse a "(U//SSI)" marking', function () {
            var expected = [{
                classification: {
                    markingType: 'us',
                    us: {
                        marking: 'U'
                    }
                },
                nonic: {
                    ssi: true
                }
            }];
            try {
                parser.feed("(U//SSI)");
                var actual = parser.results;
                assert.deepEqual(actual, expected);
            } catch (parseError) {
                throw new Error(parseError);
            }
        });
    });
});
