// Author: Steffen T Kory
// Date: 2017-05-19
// Version: 0.0.1-SNAPSHOT
// Licensed under the MIT License. See LICENSE file in the project root for full license information.
const gulp = require('gulp');
const babel = require('gulp-babel');
const exec = require('child_process').exec;

gulp.task('build', function() {
  var command = "nearleyc grammer/capco.ne -o build/capco.js";
  exec(command, function (error, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
  });
});

gulp.task('babel', function() {
  gulp.src('build/capco.js')
        .pipe(babel({ presets: ['env'] }))
        .pipe(gulp.dest('lib'));
});

gulp.task('test', function() {
  var command = "mocha --require babel-polyfill --compilers js:babel-register --colors";
  exec(command, function (error, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
  });
});
