"use strict";

const util = require('util');

var mapping = {
    "HCS": "hcs",
    "KLONDIKE": "kdk",
    "KDK": "kdk",
    "RESERVE": "rsv",
    "RSV": "rsv",
    "SI": "si",
    "SI-G": "si-g",
    "SI-GAMMA": "si-g",
    "TALENT KEYHOLE": "tk",
    "TK": "tk",
    "FOREIGN GOVERNMENT INFORMATION": "fgi",
    "FGI": "fgi",
    "RS": "rsen",
    "FOUO": "fouo",
    "OC": "orcon",
    "IMC": "imcon",
    "NF": "noforn",
    "PR": "propin",
    "REL TO": "relto",
    "REL": "relto",
    "RELIDO": "relido",
    "USA/____EYES ONLY": "eyes",
    "EYES": "eyes",
    "DSEN": "dsen",
    "FISA": "fisa",
    "DISPLAY ONLY": "display",
    "RISK SENSITIVE": "rsen",
    "RSEN": "rsen",
    "FOR OFFICIAL USE ONLY": "fouo",
    "ORIGINATOR CONTROLLED": "orcon",
    "ORCON": "orcon",
    "CONTROLLED IMAGERY": "imcon",
    "IMCON": "imcon",
    "NOT RELEASABLE TO FOREIGN NATIONALS": "noforn",
    "NOFORN": "noforn",
    "CAUTION-PROPRIETARY INFORMATION INVOLVED": "propin",
    "PROPIN": "propin",
    "AUTHORIZED FOR RELEASE TO": "relto",
    "RELEASABLE BY INFORMATION DISCLOSURE OFFICIAL": "relido",
    "DEA SENSITIVE": "dsen",
    "FOREIGN INTELLIGENCE SURVEILLANCE ACT": "fisa",
    "DS": "limdis",
    "LIMITED DISTRIBUTION": "limdis",
    "LIMDIS": "limdis",
    "XD": "exdis",
    "EXCLUSIVE DISTRIBUTION": "exdis",
    "EXDIS": "exdis",
    "ND": "nodis",
    "NO DISTRIBUTION": "nodis",
    "NODIS": "nodis",
    "SBU": "sbu",
    "SENSITIVE BUT UNCLASSIFIED": "sbu",
    "SBU-NF": "sbunf",
    "SENSITIVE BUT UNCLASSIFIED NOFORN": "sbunf",
    "SBU NOFORN": "sbunf",
    "LES": "les",
    "LAW ENFORCEMENT SENSITIVE": "les",
    "LES-NF": "lesnf",
    "LAW ENFORCEMENT SENSITIVE NOFORN": "lesnf",
    "LES NOFORN": "lesnf",
    "SSI": "ssi",
    "SENSITIVE SECURITY INFORMATION": "ssi",
};

var compartment = function (d) {
    let compartment = d.join('');

    return compartment;
};

var compartments = function (d) {
    let list = [];

    d[0].forEach(function (item, index, array) {
        let compartment = item[0];
        list.push(compartment);
    });
    return {list: list};
};

var countries = function (d) {
    var list = [];

    d[0].forEach(function (item, index, array) {
        list.push(item[2][0]);
    });
    return {list: list};
};

var sci = function (d) {
    let object = {};
    let key = mapping[d[1][0]];

    if (d[2]) {
        object[key] = {};
        object[key]['compartments'] = d[2].list;
    } else {
        object[key] = true;
    }

    return object;
};

var dissemination = function (d) {
    let object = {};
    let key = mapping[d[1][0]];
    if (d[1][2]) {
        object[key] = {};
        object[key]['countries'] = d[1][2][0].list;
    } else {
        object[key] = true;
    }
    return object;
};

var nonIc = function (d) {
    let object = {};
    let key = mapping[d[1][0]];
    object[key] = true;
    return object;
};

var joint = function (d) {
    let object = {};

    object['markingType'] = 'joint';
    object[object['markingType']] = {
        marking: d[3][0],
        countries: d[5].list
    };

    return object;
};

var nonus = function (d) {
    let object = {};

    object['markingType'] = 'nonUs';
    object[object['markingType']] = {
        marking: d[3][0],
        countries: d[1].list
    };

    return object;
};

var fgiNonus = function (d) {
    let object = {};

    object['markingType'] = 'nonUs';
    object[object['markingType']] = {
        marking: d[4][0],
        countries: d[6].list
    };

    return object;
};

var capco = function (d) {
    let object = {};
    object['classification'] = _getClassification(d[0]);
    _merge(object, 'sci', d[1]);
    _merge(object, 'sap', d[2]);
    _merge(object, 'aea', d[3]);
    _merge(object, 'fgi', d[4]);
    _merge(object, 'dissemination', d[5]);
    _merge(object, 'nonic', d[6]);
    return object;
};

var content = function (d) {
    return d[1];
};

var _getClassification = d => {
    let object = {};
    if (["TOP SECRET", "SECRET", "CONFIDENTIAL", "UNCLASSIFIED", "TS", "S", "C", "U"].indexOf(d) >= 0) {
        object['markingType'] = 'us';
        object[object['markingType']] = {
            marking: d
        };
    } else {
        object = d;
    }
    return object;
};

var _merge = (object, categoryKey, d) => {
    let category = {};

    d.forEach(function (item, index, array) {
        if (item) {
            let keys = Object.keys(item);
            keys.forEach(function (key) {
                category[key] = item[key];
            });
        }
    });

    if ((Object.keys(category)).length > 0) {
        object[categoryKey] = category;
    }

};

exports.CapcoUtilities = {
    compartment: compartment,
    compartments: compartments,
    countries: countries,
    sci: sci,
    dissemination: dissemination,
    nonIc: nonIc,
    joint: joint,
    nonus: nonus,
    fgiNonus: fgiNonus,
    capco: capco,
    content: content
};