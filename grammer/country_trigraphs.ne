# Author: Steffen T Kory
# Date: 2017-05-19
# Version: 0.0.1-SNAPSHOT
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

TRIGRAPH -> "AFG" |
  "XQZ" |
  "ALB" |
  "DZA" |
  "ASM" |
  "AND" |
  "AGO" |
  "AIA" |
  "ATA" |
  "ATG" |
  "ARG" |
  "ARM" |
  "ABW" |
  "XAC" |
  "AUS" |
  "AUT" |
  "AZE" |
  "BHS" |
  "BHR" |
  "XBK" |
  "BGD" |
  "BRB" |
  "XBI" |
  "BLR" |
  "BEL" |
  "BLZ" |
  "BEN" |
  "BMU" |
  "BTN" |
  "BOL" |
  "BES" |
  "BIH" |
  "BWA" |
  "BVT" |
  "BRA" |
  "IOT" |
  "BRN" |
  "BGR" |
  "BFA" |
  "MMR" |
  "BDI" |
  "CPV" |
  "KHM" |
  "CMR" |
  "CAN" |
  "CYM" |
  "CAF" |
  "TCD" |
  "CHL" |
  "CHN" |
  "CXR" |
  "CPT" |
  "CCK" |
  "COL" |
  "COM" |
  "COG" |
  "COD" |
  "COK" |
  "XCS" |
  "CRI" |
  "CIV" |
  "HRV" |
  "CUB" |
  "CUW" |
  "CYP" |
  "CZE" |
  "DNK" |
  "XXD" |
  "DGA" |
  "DJI" |
  "DMA" |
  "DOM" |
  "ECU" |
  "EGY" |
  "SLV" |
  "XAZ" |
  "XCR" |
  "XCY" |
  "XKM" |
  "XKN" |
  "AX3" |
  "GNQ" |
  "ERI" |
  "EST" |
  "ETH" |
  "XEU" |
  "FLK" |
  "FRO" |
  "FJI" |
  "FIN" |
  "FRA" |
  "GUF" |
  "PYF" |
  "ATF" |
  "GAB" |
  "GMB" |
  "XGZ" |
  "GEO" |
  "DEU" |
  "GHA" |
  "GIB" |
  "XGL" |
  "GRC" |
  "GRL" |
  "GRD" |
  "GLP" |
  "GUM" |
  "AX2" |
  "GTM" |
  "GGY" |
  "GIN" |
  "GNB" |
  "GUY" |
  "HTI" |
  "HMD" |
  "VAT" |
  "HND" |
  "HKG" |
  "XHO" |
  "HUN" |
  "ISL" |
  "IND" |
  "IDN" |
  "IRN" |
  "IRQ" |
  "IRL" |
  "IMN" |
  "ISR" |
  "ITA" |
  "JAM" |
  "XJM" |
  "JPN" |
  "XJV" |
  "JEY" |
  "XJA" |
  "JOR" |
  "XJN" |
  "KAZ" |
  "KEN" |
  "XKR" |
  "KIR" |
  "PRK" |
  "KOR" |
  "XKS" |
  "KWT" |
  "KGZ" |
  "LAO" |
  "LVA" |
  "LBN" |
  "LSO" |
  "LBR" |
  "LBY" |
  "LIE" |
  "LTU" |
  "LUX" |
  "MAC" |
  "MKD" |
  "MDG" |
  "MWI" |
  "MYS" |
  "MDV" |
  "MLI" |
  "MLT" |
  "MHL" |
  "MTQ" |
  "MRT" |
  "MUS" |
  "MYT" |
  "MEX" |
  "FSM" |
  "XMW" |
  "MDA" |
  "MCO" |
  "MNG" |
  "MNE" |
  "MSR" |
  "MAR" |
  "MOZ" |
  "NAM" |
  "NRU" |
  "XNV" |
  "NPL" |
  "NLD" |
  "NCL" |
  "NZL" |
  "NIC" |
  "NER" |
  "NGA" |
  "NIU" |
  "NFK" |
  "MNP" |
  "NOR" |
  "OMN" |
  "PAK" |
  "PLW" |
  "XPL" |
  "PAN" |
  "PNG" |
  "XPR" |
  "PRY" |
  "PER" |
  "PHL" |
  "PCN" |
  "POL" |
  "PRT" |
  "PRI" |
  "QAT" |
  "REU" |
  "ROU" |
  "RUS" |
  "RWA" |
  "BLM" |
  "SHN" |
  "KNA" |
  "LCA" |
  "MAF" |
  "SPM" |
  "VCT" |
  "WSM" |
  "SMR" |
  "STP" |
  "SAU" |
  "SEN" |
  "SRB" |
  "SYC" |
  "SLE" |
  "SGP" |
  "SXM" |
  "SVK" |
  "SVN" |
  "SLB" |
  "SOM" |
  "ZAF" |
  "SGS" |
  "SSD" |
  "ESP" |
  "XSP" |
  "LKA" |
  "SDN" |
  "SUR" |
  "XSV" |
  "SWZ" |
  "SWE" |
  "CHE" |
  "SYR" |
  "TWN" |
  "TJK" |
  "TZA" |
  "THA" |
  "TLS" |
  "TGO" |
  "TKL" |
  "TON" |
  "TTO" |
  "XTR" |
  "TUN" |
  "TUR" |
  "TKM" |
  "TCA" |
  "TUV" |
  "UGA" |
  "UKR" |
  "ARE" |
  "GBR" |
  "USA" |
  "AX1" |
  "URY" |
  "UZB" |
  "VUT" |
  "VAT" |
  "VEN" |
  "VNM" |
  "VGB" |
  "VIR" |
  "XWK" |
  "WLF" |
  "XWB" |
  "ESH" |
  "YEM" |
  "ZMB" |
  "ZWE"
