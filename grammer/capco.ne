# Author: Steffen T Kory
# Date: 2017-05-17
# Version: 0.0.1-SNAPSHOT
# Licensed under the MIT License. See LICENSE file in the project root for
# full license information.

@builtin "whitespace.ne"
@include "countries.ne"
@include "country_trigraphs.ne"

CAPCO   -> BANNER  {% id %} |
           PORTION {% id %}

PORTION -> P_START P_CONTENT P_END {% d => CapcoUtilities.content(d) %}

BANNER  ->
  "TOP SECRET"       SCI  SAP:* AEA:* FGI:*  DISSEMINATION NONIC:* {% d => CapcoUtilities.capco(d) %} |
  "SECRET"           SCI  SAP:* AEA:* FGI:*  DISSEMINATION NONIC:* {% d => CapcoUtilities.capco(d) %} |
  "CONFIDENTIAL"     SCI  SAP:* AEA:* FGI:*  DISSEMINATION NONIC:* {% d => CapcoUtilities.capco(d) %} |
  "UNCLASSIFIED"     NULL NULL  AEA:* FGI:*  DISSEMINATION NONIC:* {% d => CapcoUtilities.capco(d) %} |
  JOINT_CLASSIFIED   SCI  SAP:* NULL  NULL   (RELTO:*)     NULL    {% d => CapcoUtilities.capco(d) %} |
  JOINT_UNCLASSIFIED NULL NULL  NULL  NULL   (RELTO:*)     NULL    {% d => CapcoUtilities.capco(d) %} |
  NONUS_CLASSIFIED   SCI  SAP:* AEA:* NULL   DISSEMINATION NONIC:* {% d => CapcoUtilities.capco(d) %} |
  NONUS_UNCLASSIFIED NULL NULL  AEA:* NULL   DISSEMINATION NONIC:* {% d => CapcoUtilities.capco(d) %} |
  FGI_CLASSIFIED     SCI  SAP:* AEA:* NULL   DISSEMINATION NONIC:* {% d => CapcoUtilities.capco(d) %} |
  FGI_UNCLASSIFIED   NULL NULL  AEA:* NULL   DISSEMINATION NONIC:* {% d => CapcoUtilities.capco(d) %}

P_CONTENT ->
  "TS"    P_SCI P_SAP:* P_AEA:* P_FGI:* P_DISSEMINATION P_NONIC:* {% d => CapcoUtilities.capco(d) %} |
  "S"     P_SCI P_SAP:* P_AEA:* P_FGI:* P_DISSEMINATION P_NONIC:* {% d => CapcoUtilities.capco(d) %} |
  "C"     P_SCI P_SAP:* P_AEA:* P_FGI:* P_DISSEMINATION P_NONIC:* {% d => CapcoUtilities.capco(d) %} |
  "U"     NULL  NULL    P_AEA:* P_FGI:* P_DISSEMINATION P_NONIC:* {% d => CapcoUtilities.capco(d) %} |
  JC      P_SCI P_SAP:* NULL    NULL    (REL:*)         NULL      {% d => CapcoUtilities.capco(d) %} |
  JU      NULL  NULL    NULL    NULL    (REL:*)         NULL      {% d => CapcoUtilities.capco(d) %} |
  NONUS_C P_SCI P_SAP:* P_AEA:* NULL    P_DISSEMINATION P_NONIC:* {% d => CapcoUtilities.capco(d) %} |
  NONUS_U NULL  NULL    P_AEA:* NULL    P_DISSEMINATION P_NONIC:* {% d => CapcoUtilities.capco(d) %} |
  FGI_C   P_SCI P_SAP:* P_AEA:* NULL    P_DISSEMINATION P_NONIC:* {% d => CapcoUtilities.capco(d) %} |
  FGI_U   NULL  NULL    P_AEA:* NULL    P_DISSEMINATION P_NONIC:* {% d => CapcoUtilities.capco(d) %}

# JOINT classifications
JOINT_CLASSIFIED -> SEPARATOR "JOINT" __ ("TOP SECRET" | "SECRET" | "CONFIDENTIAL") __ COUNTRY_TRIGRAPHS {% d => CapcoUtilities.joint(d) %}
JOINT_UNCLASSIFIED -> SEPARATOR "JOINT" __ ("UNCLASSIFIED") __ COUNTRY_TRIGRAPHS                         {% d => CapcoUtilities.joint(d) %}
JC -> SEPARATOR "JOINT" __ ("TS" | "S" | "C") __ COUNTRY_TRIGRAPHS                                       {% d => CapcoUtilities.joint(d) %}
JU -> SEPARATOR "JOINT" __ ("U") __ COUNTRY_TRIGRAPHS                                                    {% d => CapcoUtilities.joint(d) %}

# NON-US classifications
NONUS_CLASSIFIED   -> SEPARATOR (COUNTRY_TRIGRAPHS | COUNTRIES) __ ("TOP SECRET" | "SECRET" | "CONFIDENTIAL") {% d => CapcoUtilities.nonus(d) %}
NONUS_UNCLASSIFIED -> SEPARATOR (COUNTRY_TRIGRAPHS | COUNTRIES) __ ("UNCLASSIFIED")                           {% d => CapcoUtilities.nonus(d) %}
NONUS_C            -> SEPARATOR (COUNTRY_TRIGRAPHS | COUNTRIES) __ ("TS" | "S" | "C")                         {% d => CapcoUtilities.nonus(d) %}
NONUS_U            -> SEPARATOR (COUNTRY_TRIGRAPHS | COUNTRIES) __ ("U")                                      {% d => CapcoUtilities.nonus(d) %}

# FGI NON-US classifications
FGI_CLASSIFIED     -> SEPARATOR "FGI" __ ("TOP SECRET" | "SECRET" | "CONFIDENTIAL") __ (COUNTRY_TRIGRAPHS | COUNTRIES) {% d => CapcoUtilities.fgiNonus(d) %}
FGI_UNCLASSIFIED   -> SEPARATOR "FGI" __ ("UNCLASSIFIED") __ (COUNTRY_TRIGRAPHS | COUNTRIES)                           {% d => CapcoUtilities.fgiNonus(d) %}
FGI_C              -> SEPARATOR "FGI" __ ("TS" | "S" | "C") __ (COUNTRY_TRIGRAPHS | COUNTRIES)                         {% d => CapcoUtilities.fgiNonus(d) %}
FGI_U              -> SEPARATOR "FGI" __ ("U") __ (COUNTRY_TRIGRAPHS | COUNTRIES)                                      {% d => CapcoUtilities.fgiNonus(d) %}

# SCI
SCI   -> HCS:? KDK:? RSV:? SI:? TK:?
HCS   -> SEPARATOR ("HCS")                    SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %}
KDK   -> SEPARATOR ("KLONDIKE"       | "KDK") SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %}
RSV   -> SEPARATOR ("RESERVE"        | "RSV") SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %}
SI    -> SEPARATOR ("SI")                                                      {% d => CapcoUtilities.sci(d) %} |
         SEPARATOR ("SI")                     SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %} |
         SEPARATOR ("SI-G" | "SI-GAMMA")      SUBCOMPARTMENT:* SCI_COMPARTMENT {% d => CapcoUtilities.sci(d) %}
TK    -> SEPARATOR ("TALENT KEYHOLE" | "TK")  SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %}

# PORTION SCI
P_SCI   -> P_HCS:? P_KDK:? P_RSV:? P_SI:? P_TK:?
P_HCS   -> SEPARATOR ("HCS")  SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %}
P_KDK   -> SEPARATOR ("KDK")  SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %}
P_RSV   -> SEPARATOR ("RSV")  SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %}
P_SI    -> SEPARATOR ("SI")                                    {% d => CapcoUtilities.sci(d) %} |
           SEPARATOR ("SI")   SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %} |
           SEPARATOR ("SI-G") SUBCOMPARTMENT:* SCI_COMPARTMENT {% d => CapcoUtilities.sci(d) %}
P_TK    -> SEPARATOR ("TK")   SCI_COMPARTMENT                  {% d => CapcoUtilities.sci(d) %}

# SAP
SAP   -> "SAP"
P_SAP -> "SAP"

# AEA
AEA   -> "AEA"
P_AEA -> "AEA"

# BANNER and PORTION FGI
FGI   -> SEPARATOR "FOREIGN GOVERNMENT INFORMATION" __ (COUNTRY_TRIGRAPHS | COUNTRIES) |
         SEPARATOR "FGI" __ (COUNTRY_TRIGRAPHS | COUNTRIES)                            |
         SEPARATOR "FGI"
P_FGI -> SEPARATOR "FGI" __ (COUNTRY_TRIGRAPHS) |
         SEPARATOR "FGI"

# PORTION DISSEMINATION
P_DISSEMINATION -> RS:? P_FOUO:? OC:? IMC:? NF:? PR:? REL:? P_RELIDO:? P_EYES:? P_DSEN:? P_FISA:? P_DISPLAY:?
RS              -> SEPARATOR ("RS")                                            {% d => CapcoUtilities.dissemination(d) %}
P_FOUO          -> SEPARATOR ("FOUO")                                          {% d => CapcoUtilities.dissemination(d) %}
OC              -> SEPARATOR ("OC")                                            {% d => CapcoUtilities.dissemination(d) %}
IMC             -> SEPARATOR ("IMC")                                           {% d => CapcoUtilities.dissemination(d) %}
NF              -> SEPARATOR ("NF")                                            {% d => CapcoUtilities.dissemination(d) %}
PR              -> SEPARATOR ("PR")                                            {% d => CapcoUtilities.dissemination(d) %}
REL             -> SEPARATOR ("REL TO" __ (COUNTRY_TRIGRAPHS)        |
                              "REL" __ (COUNTRY_TRIGRAPHS)           |
                              "REL")                                           {% d => CapcoUtilities.dissemination(d) %}
P_RELIDO        -> SEPARATOR ("RELIDO")                                        {% d => CapcoUtilities.dissemination(d) %}
P_EYES          -> SEPARATOR ("USA/____EYES ONLY"                    | "EYES") {% d => CapcoUtilities.dissemination(d) %}
P_DSEN          -> SEPARATOR ("DSEN")                                          {% d => CapcoUtilities.dissemination(d) %}
P_FISA          -> SEPARATOR ("FISA")                                          {% d => CapcoUtilities.dissemination(d) %}
P_DISPLAY       -> SEPARATOR ("DISPLAY ONLY" __ (COUNTRY_TRIGRAPHS))           {% d => CapcoUtilities.dissemination(d) %}

# BANNER DISSEMINATION
DISSEMINATION -> RSEN:? FOUO:? ORCON:? IMCON:? NOFORN:? PROPIN:? RELTO:? RELIDO:? EYES:? DSEN:? FISA:? DISPLAY:?
RSEN          -> SEPARATOR ("RISK SENSITIVE"                                               | "RSEN")                                            {% d => CapcoUtilities.dissemination(d) %}
FOUO          -> SEPARATOR ("FOR OFFICIAL USE ONLY"                                        | "FOUO")                                            {% d => CapcoUtilities.dissemination(d) %}
ORCON         -> SEPARATOR ("ORIGINATOR CONTROLLED"                                        | "ORCON")                                           {% d => CapcoUtilities.dissemination(d) %}
IMCON         -> SEPARATOR ("CONTROLLED IMAGERY"                                           | "IMCON")                                           {% d => CapcoUtilities.dissemination(d) %}
NOFORN        -> SEPARATOR ("NOT RELEASABLE TO FOREIGN NATIONALS"                          | "NOFORN")                                          {% d => CapcoUtilities.dissemination(d) %}
PROPIN        -> SEPARATOR ("CAUTION-PROPRIETARY INFORMATION INVOLVED"                     | "PROPIN")                                          {% d => CapcoUtilities.dissemination(d) %}
RELTO         -> SEPARATOR ("AUTHORIZED FOR RELEASE TO" __ (COUNTRY_TRIGRAPHS | COUNTRIES) | "REL TO" __ (COUNTRY_TRIGRAPHS | COUNTRIES))       {% d => CapcoUtilities.dissemination(d) %}
RELIDO        -> SEPARATOR ("RELEASABLE BY INFORMATION DISCLOSURE OFFICIAL"                | "RELIDO")                                          {% d => CapcoUtilities.dissemination(d) %}
EYES          -> SEPARATOR ("USA/____EYES ONLY")                                                                                                {% d => CapcoUtilities.dissemination(d) %}
DSEN          -> SEPARATOR ("DEA SENSITIVE")                                                                                                    {% d => CapcoUtilities.dissemination(d) %}
FISA          -> SEPARATOR ("FOREIGN INTELLIGENCE SURVEILLANCE ACT"                        | "FISA")                                            {% d => CapcoUtilities.dissemination(d) %}
DISPLAY       -> SEPARATOR ("DISPLAY ONLY" __ (COUNTRY_TRIGRAPHS | COUNTRIES)              | "DISPLAY ONLY" __ (COUNTRY_TRIGRAPHS | COUNTRIES)) {% d => CapcoUtilities.dissemination(d) %}

# PORTION NONIC
P_NONIC     -> DS | XD | ND | P_SBU | SBUNF | P_LES | LESNF | P_SSI
DS          -> SEPARATOR ("DS")     {% d => CapcoUtilities.nonIc(d) %}
XD          -> SEPARATOR ("XD")     {% d => CapcoUtilities.nonIc(d) %}
ND          -> SEPARATOR ("ND")     {% d => CapcoUtilities.nonIc(d) %}
P_SBU       -> SEPARATOR ("SBU")    {% d => CapcoUtilities.nonIc(d) %}
SBUNF       -> SEPARATOR ("SBU-NF") {% d => CapcoUtilities.nonIc(d) %}
P_LES       -> SEPARATOR ("LES")    {% d => CapcoUtilities.nonIc(d) %}
LESNF       -> SEPARATOR ("LES-NF") {% d => CapcoUtilities.nonIc(d) %}
P_SSI       -> SEPARATOR ("SSI")    {% d => CapcoUtilities.nonIc(d) %}

# BANNER NONIC
NONIC     -> LIMDIS | EXDIS | NODIS | SBU | SBUNOFORN | LES | LESNOFORN | SSI
LIMDIS    -> SEPARATOR ("LIMITED DISTRIBUTION"              | "LIMDIS")     {% d => CapcoUtilities.nonIc(d) %}
EXDIS     -> SEPARATOR ("EXCLUSIVE DISTRIBUTION"            | "EXDIS")      {% d => CapcoUtilities.nonIc(d) %}
NODIS     -> SEPARATOR ("NO DISTRIBUTION"                   | "NODIS")      {% d => CapcoUtilities.nonIc(d) %}
SBU       -> SEPARATOR ("SENSITIVE BUT UNCLASSIFIED"        | "SBU" )       {% d => CapcoUtilities.nonIc(d) %}
SBUNOFORN -> SEPARATOR ("SENSITIVE BUT UNCLASSIFIED NOFORN" | "SBU NOFORN") {% d => CapcoUtilities.nonIc(d) %}
LES       -> SEPARATOR ("LAW ENFORCEMENT SENSITIVE"         | "LES")        {% d => CapcoUtilities.nonIc(d) %}
LESNOFORN -> SEPARATOR ("LAW ENFORCEMENT SENSITIVE NOFORN"  | "LES NOFORN") {% d => CapcoUtilities.nonIc(d) %}
SSI       -> SEPARATOR ("SENSITIVE SECURITY INFORMATION"    | "SSI")        {% d => CapcoUtilities.nonIc(d) %}

COUNTRY_TRIGRAPHS -> (COMMA:? _ TRIGRAPH):+ {% d => CapcoUtilities.countries(d) %}
COUNTRIES         -> (COMMA:? _ COUNTRY):+  {% d => CapcoUtilities.countries(d) %}

SCI_COMPARTMENT -> (COMPARTMENT SUBCOMPARTMENT:*):* {% d => CapcoUtilities.compartments(d) %}
COMPARTMENT     -> DASH [A-Z] [A-Z] [A-Z] {% d => CapcoUtilities.compartment(d) %}
SUBCOMPARTMENT  -> __ [A-Z] [A-Z] [A-Z] [A-Z]
PROGRAM         -> DASH [A-Z] [A-Z] [A-Z]

NULL            -> null          {% null %}
DASH            -> "-"           {% null %}
SEPARATOR       -> ("//" | "/")  {% null %}
COMMA           -> ","           {% null %}
P_START         -> "("           {% null %}
P_END           -> ")"           {% null %}

@{%
  var CapcoUtilities = require('./capco_utilities').CapcoUtilities;
%}
